#!/bin/bash

if [[ $# -ne 1 ]]
then
    echo "Veuillez passer un argument contenant l’arbre à afficher sous forme de liste"
    exit 1
fi

tempdir=$(mktemp -d)
cat > "$tempdir/tree.tex" <<-BASH
	\\documentclass{standalone}
	\\usepackage[utf8]{inputenc}
	\\usepackage{forest}
	\\usepackage{underscore}
	
	\\begin{document}
	\\begin{forest}
	$1
	\\end{forest}
	\\end{document}
BASH
latexmk -pdf "$tempdir/tree.tex" -outdir="$tempdir" -interaction=nonstopmode >/dev/null 2>&1
open "$tempdir/tree.pdf" 2>/dev/null || xdg-open "$tempdir/tree.pdf" 2>/dev/null || true
