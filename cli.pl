/* File: cli.pl
   Purpose: Interface utilisateur pour la saisie des phrases
   à analyser.
*/

:- use_module('analyse').
:- use_module('tools').
:- use_module('dictionnaire').

% Boucle principale demandant la saisie d’une phrase et
% affichant son arbre syntaxique en cas de succès
main :-
    ansi_format([bold], "Entrez une phrase :\n> ", []),
    read_string(user_input, "\n", "", _, Phrase),
    (
        member(Phrase, ["", "q", "quit", "exit", ":q"])
            -> true % sortie du programme
            ; coupe_phrase(Phrase, Mots),
              décontracte(Mots, MotsDécontractés),
              vérif_mots_connus(MotsDécontractés),
              analyse_mots(Mots),
              write("\n\n"),
              main
    ).

% Vérifie que tous les mots d'une liste de mots soient présents
% dans le dictionnaire et demande à l’utilisateur de renseigner les
% mots manquants
vérif_mots_connus([]).

vérif_mots_connus([Mot|Reste]) :-
    mot(Mot),
    vérif_mots_connus(Reste).

vérif_mots_connus([Mot|Reste]) :-
    ansi_format([fg(yellow)],
        "\nAjout du mot inconnu « ~s » (laisser vide pour annuler).\n", [Mot]),
    ajout(Mot)
        -> vérif_mots_connus(Reste)
        ; ansi_format([fg(red)], "Annulation.\n", []).

% Lance l'analyse syntaxique d'une phrase et affiche l'arbre résultant
% en cas de succès. Sinon, affiche un message d'erreur
analyse_mots(Mots) :-
    analyse(Mots, Arbre)
        -> write(Arbre)
        ; ansi_format([fg(red)], "Cette phrase est incorrecte.", []).
