/* File: dictionnaire.pl
   Purpose: Définition des catégories lexicales pour un dictionnaire
   de mots.
*/

:- module('dictionnaire', [
    ajout/1,
    mot/1,
    entre/3,
    pronom_relatif/3,
    pronom_personnel/5,
    déterminant/4,
    préposition/2,
    nom/6,
    verbe/5,
    décontracte/2
]).

:- use_module('tools').

% Catégories de mots extensibles à l’exécution
:- dynamic(pronom_relatif/3).
:- dynamic(pronom_personnel/5).
:- dynamic(déterminant/4).
:- dynamic(préposition/2).
:- dynamic(nom/6).
:- dynamic(verbe/5).

% Ajout interactif d'un mot dans le dictionnaire
ajout(Mot) :-
    format("Catégorie lexicale ?\n", []),
    choix([
        pronom_relatif: "pronom relatif",
        pronom_personnel: "pronom personnel",
        déterminant: "déterminant",
        préposition: "préposition",
        nom: "nom",
        verbe: "verbe"
    ], Catégorie),
    insérer(Catégorie, Mot).

insérer(pronom_relatif, Mot) :-
    format("\nType ?\n", []),
    choix([
        sujet: "sujet",
        objet: "objet",
        _: "les deux"
    ], Type),
    assert((pronom_relatif(D, F, Type) :- entre(Mot, D, F))).

insérer(pronom_personnel, Mot) :-
    format("\nPersonne ?\n", []),
    choix([
        1: "première personne",
        2: "deuxième personne",
        3: "troisième personne"
    ], Personne),
    format("\nNombre ?\n", []),
    choix([
        sing: "singulier",
        plur: "pluriel",
        _: "les deux"
    ], Nombre),
    format("\nGenre ?\n", []),
    choix([
        fem: "féminin",
        mas: "masculin",
        _: "les deux"
    ], Genre),
    assert((pronom_personnel(D, F, Personne, Nombre, Genre) :- entre(Mot, D, F))).

insérer(déterminant, Mot) :-
    format("\nNombre ?\n", []),
    choix([
        sing: "singulier",
        plur: "pluriel",
        _: "les deux"
    ], Nombre),
    format("\nGenre ?\n", []),
    choix([
        fem: "féminin",
        mas: "masculin",
        _: "les deux"
    ], Genre),
    assert((déterminant(D, F, Nombre, Genre) :- entre(Mot, D, F))).

insérer(préposition, Mot) :-
    assert((préposition(D, F) :- entre(Mot, D, F))).

insérer(nom, Mot) :-
    format("\nType ?\n", []),
    choix([
        commun: "commun",
        propre: "propre",
        _: "les deux"
    ], Type),
    format("\nEst animé ?\n", []),
    choix([
        0: "inanimé",
        3: "animé",
        _: "les deux"
    ], Personne),
    format("\nNombre ?\n", []),
    choix([
        sing: "singulier",
        plur: "pluriel",
        _: "les deux"
    ], Nombre),
    format("\nGenre ?\n", []),
    choix([
        fem: "féminin",
        mas: "masculin",
        _: "les deux"
    ], Genre),
    assert((nom(D, F, Type, Personne, Nombre, Genre) :- entre(Mot, D, F))).

insérer(verbe, Mot) :-
    format("\nType ?\n", []),
    choix([
        trans: "transitif",
        intrans: "intransitif",
        _: "les deux"
    ], Type),
    format("\nPersonne ?\n", []),
    choix([
        1: "première personne",
        2: "deuxième personne",
        3: "troisième personne"
    ], Personne),
    format("\nNombre ?\n", []),
    choix([
        sing: "singulier",
        plur: "pluriel",
        _: "les deux"
    ], Nombre),
    assert((verbe(D, F, Type, Personne, Nombre) :- entre(Mot, D, F))).

% Vérifie si un mot est enregistré dans le dictionaire
mot(Mot) :- pronom_relatif([Mot], [], _), !.
mot(Mot) :- pronom_personnel([Mot], [], _, _, _), !.
mot(Mot) :- déterminant([Mot], [], _, _), !.
mot(Mot) :- préposition([Mot], []), !.
mot(Mot) :- nom([Mot], [], _, _, _, _), !.
mot(Mot) :- verbe([Mot], [], _, _, _), !.

% Vérifie qu’un mot correspond à une différence de listes
entre(Mot, [Mot|Reste], Reste).

% Pronoms relatifs
% Attributs :
%   - Type (sujet, objet)
pronom_relatif(D, F, sujet)             :- entre("qui", D, F).
pronom_relatif(D, F, objet)             :- entre("que", D, F).

% Pronoms personnels
% Attributs :
%   - Personne (1, 2, 3)
%   - Nombre (sing, plur)
%   - Genre (fem, mas)
pronom_personnel(D, F, 1, sing, _) :- entre("je", D, F).
pronom_personnel(D, F, 2, sing, _) :- entre("tu", D, F).
pronom_personnel(D, F, 3, sing, mas) :- entre("il", D, F).
pronom_personnel(D, F, 3, sing, fem) :- entre("elle", D, F).
pronom_personnel(D, F, 3, sing, _) :- entre("on", D, F).
pronom_personnel(D, F, 1, plur, _) :- entre("nous", D, F).
pronom_personnel(D, F, 2, plur, _) :- entre("vous", D, F).
pronom_personnel(D, F, 3, plur, mas) :- entre("ils", D, F).
pronom_personnel(D, F, 3, plur, fem) :- entre("elles", D, F).

% Déterminants
% Attributs :
%   - Nombre (sing, plur)
%   - Genre (fém, mas)
déterminant(D, F, sing, mas)    :- entre("le", D, F).
déterminant(D, F, sing, fem)    :- entre("la", D, F).
déterminant(D, F, plur, _)      :- entre("les", D, F).
déterminant(D, F, sing, mas)    :- entre("un", D, F).
déterminant(D, F, sing, fem)    :- entre("une", D, F).
déterminant(D, F, sing, _)      :- entre("mon", D, F).
déterminant(D, F, sing, _)      :- entre("ton", D, F).
déterminant(D, F, sing, _)      :- entre("son", D, F).
déterminant(D, F, sing, fem)    :- entre("ma", D, F).
déterminant(D, F, sing, fem)    :- entre("ta", D, F).
déterminant(D, F, sing, fem)    :- entre("sa", D, F).
déterminant(D, F, plur, _)      :- entre("mes", D, F).
déterminant(D, F, plur, _)      :- entre("tes", D, F).
déterminant(D, F, plur, _)      :- entre("ses", D, F).
déterminant(D, F, sing, _)      :- entre("notre", D, F).
déterminant(D, F, sing, _)      :- entre("votre", D, F).
déterminant(D, F, sing, _)      :- entre("leur", D, F).
déterminant(D, F, plur, _)      :- entre("nos", D, F).
déterminant(D, F, plur, _)      :- entre("vos", D, F).
déterminant(D, F, plur, _)      :- entre("leurs", D, F).

% Prépositions
préposition(D, F)   :- entre("de", D, F).
préposition(D, F)   :- entre("dans", D, F).
préposition(D, F)   :- entre("à", D, F).

% Noms
% Attributs :
%   - Type (commun, propre)
%   - Personne (0 pour inanimé et 3 pour animé)
%   - Nombre (sing, plur)
%   - Genre (fém, mas)
nom(D, F, commun, 3, sing, mas)    :- entre("cheval", D, F).
nom(D, F, commun, 3, plur, mas)    :- entre("chevaux", D, F).
nom(D, F, commun, 3, sing, mas)    :- entre("chat", D, F).
nom(D, F, commun, 3, plur, mas)    :- entre("chats", D, F).
nom(D, F, commun, 3, sing, mas)    :- entre("chien", D, F).
nom(D, F, commun, 3, plur, mas)    :- entre("chiens", D, F).
nom(D, F, commun, 3, sing, fem)    :- entre("chienne", D, F).
nom(D, F, commun, 3, plur, fem)    :- entre("chiennes", D, F).
nom(D, F, commun, 3, sing, mas)    :- entre("homme", D, F).
nom(D, F, commun, 3, plur, mas)    :- entre("hommes", D, F).
nom(D, F, commun, 3, sing, mas)    :- entre("copain", D, F).
nom(D, F, commun, 3, plur, mas)    :- entre("copains", D, F).
nom(D, F, commun, 3, sing, fem)    :- entre("femme", D, F).
nom(D, F, commun, 3, plur, fem)    :- entre("femmes", D, F).
nom(D, F, commun, 3, sing, fem)    :- entre("copine", D, F).
nom(D, F, commun, 3, plur, fem)    :- entre("copines", D, F).

nom(D, F, propre, 3, sing, mas)    :- entre("Pierre", D, F).
nom(D, F, propre, 3, sing, mas)    :- entre("Paul", D, F).
nom(D, F, propre, 3, sing, mas)    :- entre("Michel", D, F).
nom(D, F, propre, 3, sing, mas)    :- entre("Hervé", D, F).
nom(D, F, propre, 3, sing, mas)    :- entre("David", D, F).
nom(D, F, propre, 3, sing, mas)    :- entre("Christian", D, F).
nom(D, F, propre, 3, sing, fem)    :- entre("Anne", D, F).
nom(D, F, propre, 3, sing, fem)    :- entre("Élisabeth", D, F).
nom(D, F, propre, 3, sing, fem)    :- entre("Anne-Élisabeth", D, F).
nom(D, F, propre, 3, sing, fem)    :- entre("Marie", D, F).
nom(D, F, propre, 3, sing, fem)    :- entre("Laure", D, F).
nom(D, F, propre, 3, sing, fem)    :- entre("Marie-Laure", D, F).

nom(D, F, commun, 0, sing, mas)    :- entre("ordinateur", D, F).
nom(D, F, commun, 0, plur, mas)    :- entre("ordinateurs", D, F).
nom(D, F, commun, 0, sing, fem)    :- entre("pomme", D, F).
nom(D, F, commun, 0, plur, fem)    :- entre("pommes", D, F).
nom(D, F, commun, 0, sing, fem)    :- entre("pêche", D, F).
nom(D, F, commun, 0, plur, fem)    :- entre("pêches", D, F).
nom(D, F, commun, 0, sing, mas)    :- entre("melon", D, F).
nom(D, F, commun, 0, plur, mas)    :- entre("melons", D, F).
nom(D, F, commun, 0, sing, fem)    :- entre("orange", D, F).
nom(D, F, commun, 0, plur, fem)    :- entre("oranges", D, F).
nom(D, F, commun, 0, sing, mas)    :- entre("monde", D, F).
nom(D, F, commun, 0, plur, mas)    :- entre("mondes", D, F).
nom(D, F, commun, 0, sing, mas)    :- entre("tiroir", D, F).
nom(D, F, commun, 0, plur, mas)    :- entre("tiroirs", D, F).
nom(D, F, commun, 0, sing, fem)    :- entre("clef", D, F).
nom(D, F, commun, 0, plur, fem)    :- entre("clefs", D, F).
nom(D, F, commun, 0, sing, fem)    :- entre("clé", D, F).
nom(D, F, commun, 0, plur, fem)    :- entre("clés", D, F).
nom(D, F, commun, 0, sing, mas)    :- entre("son", D, F).
nom(D, F, commun, 0, plur, mas)    :- entre("sons", D, F).

% Verbes au présent de l’indicatif
% Attributs :
%   - Type (trans, intrans)
%   - Personne (1, 2, 3)
%   - Nombre (sing, plur)
verbe(D, F, _, 1, sing) :- entre("regarde", D, F).
verbe(D, F, _, 2, sing) :- entre("regardes", D, F).
verbe(D, F, _, 3, sing) :- entre("regarde", D, F).
verbe(D, F, _, 1, plur) :- entre("regardons", D, F).
verbe(D, F, _, 2, plur) :- entre("regardez", D, F).
verbe(D, F, _, 3, plur) :- entre("regardent", D, F).

verbe(D, F, _, 1, sing) :- entre("mange", D, F).
verbe(D, F, _, 2, sing) :- entre("manges", D, F).
verbe(D, F, _, 3, sing) :- entre("mange", D, F).
verbe(D, F, _, 1, plur) :- entre("mangeons", D, F).
verbe(D, F, _, 2, plur) :- entre("mangez", D, F).
verbe(D, F, _, 3, plur) :- entre("mangent", D, F).

verbe(D, F, _, 1, sing) :- entre("explose", D, F).
verbe(D, F, _, 2, sing) :- entre("exploses", D, F).
verbe(D, F, _, 3, sing) :- entre("explose", D, F).
verbe(D, F, _, 1, plur) :- entre("explosons", D, F).
verbe(D, F, _, 2, plur) :- entre("explosez", D, F).
verbe(D, F, _, 3, plur) :- entre("explosent", D, F).

verbe(D, F, _, 1, sing) :- entre("domine", D, F).
verbe(D, F, _, 2, sing) :- entre("domines", D, F).
verbe(D, F, _, 3, sing) :- entre("domine", D, F).
verbe(D, F, _, 1, plur) :- entre("dominons", D, F).
verbe(D, F, _, 2, plur) :- entre("dominez", D, F).
verbe(D, F, _, 3, plur) :- entre("dominent", D, F).

verbe(D, F, _, 1, sing) :- entre("suis", D, F).
verbe(D, F, _, 2, sing) :- entre("es", D, F).
verbe(D, F, _, 3, sing) :- entre("est", D, F).
verbe(D, F, _, 1, plur) :- entre("sommes", D, F).
verbe(D, F, _, 2, plur) :- entre("êtes", D, F).
verbe(D, F, _, 3, plur) :- entre("sont", D, F).

verbe(D, F, _, 1, sing) :- entre("vois", D, F).
verbe(D, F, _, 2, sing) :- entre("vois", D, F).
verbe(D, F, _, 3, sing) :- entre("voit", D, F).
verbe(D, F, _, 1, plur) :- entre("voyons", D, F).
verbe(D, F, _, 2, plur) :- entre("voyez", D, F).
verbe(D, F, _, 3, plur) :- entre("voient", D, F).

verbe(D, F, _, 1, sing) :- entre("perds", D, F).
verbe(D, F, _, 2, sing) :- entre("perds", D, F).
verbe(D, F, _, 3, sing) :- entre("perd", D, F).
verbe(D, F, _, 1, plur) :- entre("perdons", D, F).
verbe(D, F, _, 2, plur) :- entre("perdez", D, F).
verbe(D, F, _, 3, plur) :- entre("perdent", D, F).

verbe(D, F, _, 1, sing) :- entre("tombe", D, F).
verbe(D, F, _, 2, sing) :- entre("tombes", D, F).
verbe(D, F, _, 3, sing) :- entre("tombe", D, F).
verbe(D, F, _, 1, plur) :- entre("tombons", D, F).
verbe(D, F, _, 2, plur) :- entre("tombez", D, F).
verbe(D, F, _, 3, plur) :- entre("tombent", D, F).

verbe(D, F, trans, 1, sing) :- entre("attrape", D, F).
verbe(D, F, trans, 2, sing) :- entre("attrapes", D, F).
verbe(D, F, trans, 3, sing) :- entre("attrape", D, F).
verbe(D, F, trans, 1, plur) :- entre("attrapons", D, F).
verbe(D, F, trans, 2, plur) :- entre("attrapez", D, F).
verbe(D, F, trans, 3, plur) :- entre("attrapent", D, F).

verbe(D, F, trans, 1, sing) :- entre("dicte", D, F).
verbe(D, F, trans, 2, sing) :- entre("dictes", D, F).
verbe(D, F, trans, 3, sing) :- entre("dicte", D, F).
verbe(D, F, trans, 1, plur) :- entre("dictons", D, F).
verbe(D, F, trans, 2, plur) :- entre("dictez", D, F).
verbe(D, F, trans, 3, plur) :- entre("dictent", D, F).

verbe(D, F, trans, 1, sing) :- entre("écoute", D, F).
verbe(D, F, trans, 2, sing) :- entre("écoutes", D, F).
verbe(D, F, trans, 3, sing) :- entre("écoute", D, F).
verbe(D, F, trans, 1, plur) :- entre("écoutons", D, F).
verbe(D, F, trans, 2, plur) :- entre("écoutez", D, F).
verbe(D, F, trans, 3, plur) :- entre("écoutent", D, F).

verbe(D, F, trans, 1, sing) :- entre("cherche", D, F).
verbe(D, F, trans, 2, sing) :- entre("cherches", D, F).
verbe(D, F, trans, 3, sing) :- entre("cherche", D, F).
verbe(D, F, trans, 1, plur) :- entre("cherchons", D, F).
verbe(D, F, trans, 2, plur) :- entre("cherchez", D, F).
verbe(D, F, trans, 3, plur) :- entre("cherchent", D, F).

verbe(D, F, trans, 1, sing) :- entre("ai", D, F).
verbe(D, F, trans, 2, sing) :- entre("as", D, F).
verbe(D, F, trans, 3, sing) :- entre("a", D, F).
verbe(D, F, trans, 1, plur) :- entre("avons", D, F).
verbe(D, F, trans, 2, plur) :- entre("avez", D, F).
verbe(D, F, trans, 3, plur) :- entre("ont", D, F).

verbe(D, F, trans, 1, sing) :- entre("vais", D, F).
verbe(D, F, trans, 2, sing) :- entre("vas", D, F).
verbe(D, F, trans, 3, sing) :- entre("va", D, F).
verbe(D, F, trans, 1, plur) :- entre("allons", D, F).
verbe(D, F, trans, 2, plur) :- entre("allez", D, F).
verbe(D, F, trans, 3, plur) :- entre("vont", D, F).

verbe(D, F, intrans, 1, sing) :- entre("dors", D, F).
verbe(D, F, intrans, 2, sing) :- entre("dors", D, F).
verbe(D, F, intrans, 3, sing) :- entre("dort", D, F).
verbe(D, F, intrans, 1, plur) :- entre("dormons", D, F).
verbe(D, F, intrans, 2, plur) :- entre("dormez", D, F).
verbe(D, F, intrans, 3, plur) :- entre("dorment", D, F).

verbe(D, F, intrans, 1, sing) :- entre("meurs", D, F).
verbe(D, F, intrans, 2, sing) :- entre("meurs", D, F).
verbe(D, F, intrans, 3, sing) :- entre("meurt", D, F).
verbe(D, F, intrans, 1, plur) :- entre("mourons", D, F).
verbe(D, F, intrans, 2, plur) :- entre("mourez", D, F).
verbe(D, F, intrans, 3, plur) :- entre("meurent", D, F).

% Remplace les mots contractés d’une phrase par les mots qui composent
% ces contractions
décontracte(["du"|Reste], ["de"|["le"|ResteDécontracté]]) :-
    !, décontracte(Reste, ResteDécontracté).

décontracte(["des"|Reste], ["de"|["les"|ResteDécontracté]]) :-
    !, décontracte(Reste, ResteDécontracté).

décontracte(["au"|Reste], ["à"|["le"|ResteDécontracté]]) :-
    !, décontracte(Reste, ResteDécontracté).

décontracte(["aux"|Reste], ["à"|["les"|ResteDécontracté]]) :-
    !, décontracte(Reste, ResteDécontracté).

décontracte(["l"|[MotSuivant|Reste]], ["le"|[MotSuivant|ResteDécontracté]]) :-
    string_chars(MotSuivant, [PremièreLettre|_]),
    voyelle(PremièreLettre),
    décontracte(Reste, ResteDécontracté).

décontracte(["l"|[MotSuivant|Reste]], ["la"|[MotSuivant|ResteDécontracté]]) :-
    string_chars(MotSuivant, [PremièreLettre|_]),
    voyelle(PremièreLettre),
    décontracte(Reste, ResteDécontracté).

décontracte(["j"|Reste], ["je"|ResteDécontracté]) :-
    !, décontracte(Reste, ResteDécontracté).

décontracte([Tête|Reste], [Tête|ResteDécontracté]) :-
    !, décontracte(Reste, ResteDécontracté).

décontracte([], []).

voyelle(Lettre) :- member(Lettre, [a, e, h, i, o, u, y]).
