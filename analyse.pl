/* File: analyse.pl
   Purpose: DCG pour produire un arbre d’analyse syntaxique
   d’une phrase en français.
*/

:- module('analyse', [
    analyse/2
]).

:- use_module('dictionnaire').

% Analyse la syntaxe d’une phrase et reforme son arbre syntaxique
% si la phrase est valide et renvoie faux sinon
analyse(Mots, Arbre) :-
    décontracte(Mots, MotsDécontractés),
    déf_phrase(Arbre, MotsDécontractés, []).

% Phrase
déf_phrase([phrase,
    ArbreGroupeNominal,
    ArbreGroupeVerbal
], D, F) :-
    groupe_nominal(ArbreGroupeNominal, D, I, Personne, Nombre, _),
    groupe_verbal(ArbreGroupeVerbal, I, F, Personne, Nombre).

% Groupe nominal
% Attributs :
%   - Personne (0 pour inanimé, 1, 2, 3)
%   - Nombre (sing, plur)
%   - Genre (fem, mas)

%% déterminant + nom commun
groupe_nominal([groupe_nominal,
    [déterminant, [MotDéterminant]],
    [nom_commun, [MotNom]]
], D, F, Personne, Nombre, Genre) :-
    déterminant(D, I, Nombre, Genre), entre(MotDéterminant, D, I),
    nom(I, F, commun, Personne, Nombre, Genre), entre(MotNom, I, F).

%% déterminant + nom commun + relative
groupe_nominal([groupe_nominal,
    [déterminant, [MotDéterminant]],
    [nom_commun, [MotNom]],
    ArbreRelative
], D, F, Personne, Nombre, Genre) :-
    déterminant(D, I, Nombre, Genre), entre(MotDéterminant, D, I),
    nom(I, J, commun, Personne, Nombre, Genre), entre(MotNom, I, J),
    relative(ArbreRelative, J, F, Personne, Nombre).

%% nom propre
groupe_nominal([groupe_nominal,
    [nom_propre, [MotNom]]
], D, F, Personne, Nombre, Genre) :-
    nom(D, F, propre, Personne, Nombre, Genre), entre(MotNom, D, F).

%% nom propre + relative
groupe_nominal([groupe_nominal,
    [nom_propre, [MotNom]],
    ArbreRelative
], D, F, Personne, Nombre, Genre) :-
    nom(D, I, propre, Personne, Nombre, Genre), entre(MotNom, D, I),
    relative(ArbreRelative, I, F, Personne, Nombre).

%% pronom personnel
groupe_nominal([groupe_nominal,
    [pronom_personnel, [MotPPersonnel]]
], D, F, Personne, Nombre, Genre) :-
    pronom_personnel(D, F, Personne, Nombre, Genre), entre(MotPPersonnel, D, F).

% Groupe verbal
% Attributs :
%   - Personne (1, 2, 3)
%   - Nombre (sing, plur)

%% verbe intransitif
groupe_verbal([groupe_verbal,
    [verbe_intransitif, [MotVerbe]]
], D, F, Personne, Nombre) :-
    verbe(D, F, intrans, Personne, Nombre), entre(MotVerbe, D, F).

%% verbe transitif + groupe nominal
groupe_verbal([groupe_verbal,
    [verbe_transitif, [MotVerbe]],
    ArbreGroupeNominal
], D, F, Personne, Nombre) :-
    verbe(D, I, trans, Personne, Nombre), entre(MotVerbe, D, I),
    groupe_nominal(ArbreGroupeNominal, I, F, _, _, _).

%% verbe transitif + préposition + groupe nominal
groupe_verbal([groupe_verbal,
    [verbe_transitif, [MotVerbe]],
    [préposition, [MotPréposition]],
    ArbreGroupeNominal
], D, F, Personne, Nombre) :-
    verbe(D, I, trans, Personne, Nombre), entre(MotVerbe, D, I),
    préposition(I, J), entre(MotPréposition, I, J),
    groupe_nominal(ArbreGroupeNominal, J, F, _, _, _).

% Propositions relatives
% Attributs :
%   - Nombre (sing, plur)

%% avec un pronom relatif sujet
relative([relative,
    [pronom_relatif_sujet, [MotPronom]],
    ArbreGroupeVerbal
], D, F, Relative, Nombre) :-
    pronom_relatif(D, I, sujet), entre(MotPronom, D, I),
    groupe_verbal(ArbreGroupeVerbal, I, F, Relative, Nombre).

%% avec un pronom relatif COD
relative([relative,
    [pronom_relatif_objet, [MotPronom]],
    ArbreGroupeNominal,
    [verbe_transitif, [MotVerbe]]
], D, F, _, _) :-
    pronom_relatif(D, I, objet), entre(MotPronom, D, I),
    groupe_nominal(ArbreGroupeNominal, I, J, Personne, Nombre, _),
    verbe(J, F, trans, Personne, Nombre), entre(MotVerbe, J, F).

relative([relative,
    [pronom_relatif_objet, [MotPronom]],
    [verbe_transitif, [MotVerbe]],
    ArbreGroupeNominal
], D, F, _, _) :-
    pronom_relatif(D, I, objet), entre(MotPronom, D, I),
    verbe(I, J, trans, Personne, Nombre), entre(MotVerbe, I, J),
    groupe_nominal(ArbreGroupeNominal, J, F, Personne, Nombre, _).
