.PHONY: build test clean

build: build/cli

build/cli: cli.pl
	mkdir -p build
	swipl -o $@ -g main -c $^

test:
	swipl -t "load_test_files([]), run_tests." -s analyse.pl -g true

clean:
	rm -rf build
