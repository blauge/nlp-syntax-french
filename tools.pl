/* File: tools.pl
   Purpose: Outils.
*/

:- module('tools', [
    coupe_phrase/2,
    choix/2
]).

% Découpe une liste de mots délimitée par des espaces
coupe_phrase(Phrase, Mots) :-
    split_string(Phrase, " '’", " ", Mots).

% Affiche une liste de choix à l’utilisateur donnée sous la forme d’une liste
% de paires. Le premier élément de la paire est la valeur qui sera renvoyée
% par la fonction tandis que le second est la chaîne qui sera affichée à
% l’utilisateur. Si l’utilisateur laisse l’entrée vide, échec.
choix(Choix, Résultat) :-
    length(Choix, NbChoix),
    afficher_choix(Choix, 1),
    format("> "),
    read_string(user_input, "\n", "", _, Saisie),
    (number_string(Indice, Saisie); Indice = 0),
    (
        Saisie = ""
            -> fail
            ; 1 =< Indice, Indice =< NbChoix
                -> nth1(Indice, Choix, Résultat: _)
                ; ansi_format([fg(red)], "Ce choix est incorrect.\n\n", []),
                  choix(Choix, Résultat)
    ).

afficher_choix([], _).
afficher_choix([_: Affichage | Reste], Indice) :-
    format("(~d) ~s\n", [Indice, Affichage]),
    afficher_choix(Reste, Indice + 1).
