# Analyse syntaxique du français

Outil permettant de vérifier la syntaxe et d’analyser la structure de phrases en français.
Implémente une grammaire de clauses définies (DCG) dans le langage Prolog en utilisant les différences de listes.

[Présentation expliquant le fonctionnement de l’outil >](docs/slides)

## Exécution du programme

Sur Linux, Mac et dérivés :

```sh
make
```

Pour lancer la ligne de commande :

```sh
./build/cli
```

Pour lancer les tests unitaires :

```sh
make test
```

## Constructions syntaxiques supportées

* Phrase.
* Groupe nominal :
    * déterminant, nom commun potentiellement complété par une relative ;
    * nom propre potentiellement complété par une relative ;
    * pronom personnel.
* Proposition relative :
    * subordonnée (Pierre qui mange…) ;
    * indépendante de la proposition principale (Pierre que je connais bien…).
* Groupe verbal :
    * verbe transitif ;
    * verbe intransitif.
* Contractions.

## Index des fichiers

Fichier           | Description
-----------------:|------------
`analyse.pl`      | DCG pour produire un arbre d’analyse syntaxique d’une phrase en français
`analyse.plt`     | Tests unitaires pour la DCG
`dictionnaire.pl` | Définition des catégories lexicales pour un dictionnaire de mots
`cli.pl`          | Interface utilisateur pour la saisie des phrases à analyser
`tools.pl`        | Outils divers

## Interface en ligne de commande

L’exécutable `build/cli` fournit une interface en ligne de commande permettant à l’utilisateur de saisir des phrases et d’obtenir un arbre de syntaxe si elle est reconnue.
L’arbre est produit sous forme de liste.
L’utilisateur peut ensuite utiliser le script `./visualisation.sh` pour produire un PDF représentant l’arbre syntaxique.

<img src="docs/slides/fig/screenshot.png" alt="Ajout interactif d’un mot" width="473">

<img src="docs/arbre.png" alt="Exemple d’arbre produit">
