% vim: set spelllang=fr :
%! TEX program = xelatex

\documentclass[
    aspectratio=169
]{beamer}
\usepackage{slides}

% Métadonnées
\title{Outil d’analyse syntaxique du~français}
\author{
    Mattéo~\textsc{Delabre} \and
    Rémi~\textsc{Cérès}
}
\institute{HMIN230 — Traitement automatique du langage naturel}
\date{17~avril~2019}

\begin{document}

\frame[plain]{\titlepage}

\begin{frame}{Objectifs}
    Créer un outil qui~:
    \begin{itemize}
        \item Analyse quelques-unes des structures syntaxiques du français.
        \item Fournit un arbre de syntaxe pour les phrases correctes.
        \item Est accessible à partir d’une interface en ligne de commande.
        \item Permet à l’utilisateur d’ajouter de nouveaux mots.
    \end{itemize}
\end{frame}

\tikzset{
    optional/.style={
        ellipse,
        draw,
        dashed,
        inner sep=0pt,
        label={[font=\scshape\footnotesize]below:optionnelle}
    },
    numbered/.style={
        label={[circle, draw, inner sep=2pt, yshift=1pt, xshift=-2pt]left:#1}
    },
    root/.style={
        font=\bfseries\footnotesize
    }
}

\newcommand*\circled[1]{
    \tikz[baseline=(char.base)]{
        \node[
            circle,
            draw,
            inner sep=2pt
        ] (char) {#1};
    }
}

\begin{frame}{Syntaxe}{Phrase}
    \begin{tikzpicture}[overlay, remember picture]
        \fill[brunolightgray]
            ($(current page.north west)!.67!(current page.north east)$)
            rectangle (current page.south east);
    \end{tikzpicture}

    \begin{minipage}{.69\linewidth}
        Accord du groupe verbal avec le groupe nominal~:

        \begin{itemize}
            \item \textbf{en nombre~:} singulier ou pluriel~;
            \item \textbf{en personne~:}
                \begin{itemize}
                    \item inanimée (personne \enquote{zéro}),
                    \item première personne (\emph{je}),
                    \item deuxième personne (\emph{tu}),
                    \item troisième personne (\emph{il, elle} et noms animés).
                \end{itemize}
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}{.3\linewidth}
        \centering
        \begin{forest}
            [Phrase, root,
                [Groupe\\nominal],
                [Groupe\\verbal]
            ]
        \end{forest}
    \end{minipage}
\end{frame}

\begin{frame}{Syntaxe}{Groupe nominal}
    \begin{tikzpicture}[overlay, remember picture]
        \fill[brunolightgray]
            ($(current page.north west)!.525!(current page.north east)$)
            rectangle (current page.south east);
    \end{tikzpicture}

    \begin{minipage}{.45\linewidth}
        \begin{itemize}
            \item Accord entre le déterminant et le nom en nombre et en genre.
            \item Héritage du nombre et de la personne du nom en attribut du groupe nominal.
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}{.45\linewidth}
        \centering
        \vspace{-2cm}
        \begin{forest}
            [Groupe nominal, root, numbered=1,
                [Déterm.],
                [Nom\\commun],
                [Proposition\\relative, optional]
            ]
        \end{forest}

        \vspace{.75em}
        \begin{forest}
            [Groupe nominal, root, numbered=2,
                [Nom\\propre],
                [Proposition\\relative, optional]
            ]
        \end{forest}

        \vspace{.75em}
        \begin{forest}
            [Groupe nominal, root, numbered=3,
                [Pronom personnel]
            ]
        \end{forest}
    \end{minipage}
\end{frame}

\begin{frame}{Syntaxe}{Proposition relative}
    \begin{tikzpicture}[overlay, remember picture]
        \fill[brunolightgray]
            ($(current page.north west)!.525!(current page.north east)$)
            rectangle (current page.south east);
    \end{tikzpicture}

    \begin{minipage}{.45\linewidth}
        \begin{itemize}
            \item Accord du verbe avec le groupe nominal (qui précède le verbe en~\circled{2} ou la relative en~\circled{1}).
            \item En~\circled{2}, groupe nominal et verbe transitif inversibles.
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}{.45\linewidth}
        \centering
        \vspace{-2em}
        \begin{forest}
            [Proposition relative, root, numbered=1,
                [Pronom\\relatif\\sujet \emph{(qui)}],
                [Groupe\\verbal]
            ]
        \end{forest}

        \vspace{2em}
        \begin{forest}
            [Proposition relative, root, numbered=2,
                [Pronom\\relatif\\objet \emph{(que)}],
                [Groupe\\nominal, name=invert-gn],
                [Verbe\\transitif, name=invert-vt]
            ]
            \draw[<->] (invert-gn.-40) to[bend right] (invert-vt.-140);
        \end{forest}
    \end{minipage}
\end{frame}

\begin{frame}{Syntaxe}{Groupe verbal}
    \begin{tikzpicture}[overlay, remember picture]
        \fill[brunolightgray]
            ($(current page.north west)!.525!(current page.north east)$)
            rectangle (current page.south east);
    \end{tikzpicture}

    \begin{minipage}{.45\linewidth}
        \begin{itemize}
            \item Cas transitif \circled{1}~: on adjoint un complément au verbe.
            \item Une préposition (\emph{à, de, dans,} …) peut être insérée entre le verbe et le groupe nominal.
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}{.45\linewidth}
        \centering
        \vspace{-2em}
        \begin{forest}
            [Groupe verbal, root, numbered=1,
                [Verbe\\transitif],
                [Préposition, optional],
                [Groupe\\nominal]
            ]
        \end{forest}

        \vspace{2em}
        \begin{forest}
            [Groupe verbal, root, numbered=2,
                [Verbe\\intransitif]
            ]
        \end{forest}
    \end{minipage}
\end{frame}

\newcommand\contract[2]{#1 $\longrightarrow$ #2}
\begin{frame}{Syntaxe}{Contractions}
    \begin{tikzpicture}[overlay, remember picture]
        \fill[brunolightgray]
            ($(current page.north west)!.65!(current page.north east)$)
            rectangle (current page.south east);
    \end{tikzpicture}

    \begin{minipage}{.6\linewidth}
        \begin{itemize}
            \item Expansion des contractions en prétraitement.
            \item \emph{Backtracking} si la décontraction choisie amène à un échec.
            \item \textbf{Inconvénients~:} phrases non-contractées acceptées (ex.~: \enquote{je viens de le supermarché}), ambiguïté sur \enquote{du}.
            \item \textbf{Avantage~:} traitement simple et évite une catégorie hybride \emph{préposition–déterminant}.
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}{.3\linewidth}
        \begin{itemize}
            \item \contract{au}{à le}
            \item \contract{aux}{à les}
            \item \contract{l’\emph{[voyelle]}}{le, la}
            \item \contract{j’\emph{[voyelle]}}{je}
        \end{itemize}

        \vspace{1em}
        Formes ambigües avec des articles partitifs~:
        \begin{itemize}
            \item \contract{du}{de le}
            \item \contract{des}{de les}
        \end{itemize}
    \end{minipage}
\end{frame}

\begin{frame}{Interface en ligne de commande}
    \begin{minipage}{.5\linewidth}
        \begin{itemize}
            \item Boucle de saisie d’une phrase (quitter avec une phrase vide).
            \item S’il existe un mot inconnu, propose son ajout dans le dictionnaire.
            \item Si la phrase est valide, affiche son arbre de syntaxe sous forme de liste.
            \item L’arbre peut être visualisé en PDF avec un script.
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}{.4\linewidth}
        \centering
        \fbox{\includegraphics[width=\linewidth]{screenshot}}

        \vspace{.5em}
        \emph{Ajout interactif d’un mot}
    \end{minipage}
\end{frame}

\begin{frame}{Conclusion}
    % atten les 4 objectifs fixés

    \textbf{Réalisations et apports~:}
    \begin{itemize}
        \item 4 objectifs fixés (analyse, arbre, interface, ajout utilisateur).
        \item Approfondissement des connaissances sur le langage Prolog.
    \end{itemize}

    \hfill

    \textbf{Perspectives envisageables~:}
    \begin{itemize}
        \item Ajout de nouvelles catégories syntaxiques.
        \item Support du passé composé.
        \item Enrichissement du vocabulaire dans le dictionnaire.
    \end{itemize}


\end{frame}

\end{document}
