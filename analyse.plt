:- begin_tests('analyse').
:- use_module('analyse').

test('incorrect', [fail]) :-
    analyse(
        ["Marie", "regarde", "Pierre", "qui", "mangent", "une", "pomme"],
        _
    ).

test('verbe transitif 1', [nondet]) :-
    analyse(
        ["Marie", "regarde", "Pierre", "qui", "mange", "une", "pomme"],
        [phrase,
            [groupe_nominal, [nom_propre, ["Marie"]]],
            [groupe_verbal,
                [verbe_transitif, ["regarde"]],
                [groupe_nominal,
                    [nom_propre, ["Pierre"]],
                    [relative,
                        [pronom_relatif_sujet, ["qui"]],
                        [groupe_verbal,
                            [verbe_transitif, ["mange"]],
                            [groupe_nominal,
                                [déterminant, ["une"]],
                                [nom_commun, ["pomme"]]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ).

test('verbe transitif 2', [nondet]) :-
    analyse(
        ["les", "chats", "dominent", "le", "monde"],
        [phrase,
            [groupe_nominal,
                [déterminant, ["les"]],
                [nom_commun, ["chats"]]
            ],
            [groupe_verbal,
                [verbe_transitif, ["dominent"]],
                [groupe_nominal,
                    [déterminant, ["le"]],
                    [nom_commun, ["monde"]]
                ]
            ]
        ]
    ).

test('relatives 1', [nondet]) :-
    analyse(
        ["la", "chienne", "qui", "regarde", "le", "chat", "qui", "mange",
         "Pierre", "que", "Michel", "mange", "attrape", "des", "oranges",
         "que", "Marie", "explose"],
        [phrase,
            [groupe_nominal,
                [déterminant, ["la"]],
                [nom_commun, ["chienne"]],
                [relative,
                    [pronom_relatif_sujet, ["qui"]],
                    [groupe_verbal,
                        [verbe_transitif, ["regarde"]],
                        [groupe_nominal,
                            [déterminant, ["le"]],
                            [nom_commun, ["chat"]],
                            [relative,
                                [pronom_relatif_sujet, ["qui"]],
                                [groupe_verbal,
                                    [verbe_transitif, ["mange"]],
                                    [groupe_nominal,
                                        [nom_propre, ["Pierre"]],
                                        [relative,
                                            [pronom_relatif_objet, ["que"]],
                                            [groupe_nominal, [nom_propre, ["Michel"]]],
                                            [verbe_transitif, ["mange"]]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            [groupe_verbal,
                [verbe_transitif, ["attrape"]],
                [préposition, ["de"]],
                [groupe_nominal,
                    [déterminant, ["les"]],
                    [nom_commun, ["oranges"]],
                    [relative,
                        [pronom_relatif_objet, ["que"]],
                        [groupe_nominal, [nom_propre, ["Marie"]]],
                        [verbe_transitif, ["explose"]]
                    ]
                ]
            ]
        ]
    ).

test('relatives 2', [nondet]) :-
    analyse(
        ["je", "vois", "les", "clés", "que", "cherche", "Paul"],
        [phrase,
            [groupe_nominal, [pronom_personnel, ["je"]]],
            [groupe_verbal,
                [verbe_transitif, ["vois"]],
                [groupe_nominal,
                    [déterminant, ["les"]],
                    [nom_commun, ["clés"]],
                    [relative,
                        [pronom_relatif_objet, ["que"]],
                        [verbe_transitif, ["cherche"]],
                        [groupe_nominal, [nom_propre, ["Paul"]]]
                    ]
                ]
            ]
        ]
    ).

test('relatives 3', [nondet, fail]) :-
    analyse(
        ["Michel", "dicte", "à", "son", "ordinateur", "qui", "regarde", "Michel"],
        _
    ).

test('pronom personnel 1', [nondet]) :-
    analyse(
        ["les", "chevaux", "que", "vous", "mangez", "meurent"],
        [phrase,
            [groupe_nominal,
                [déterminant, ["les"]],
                [nom_commun, ["chevaux"]],
                [relative,
                    [pronom_relatif_objet, ["que"]],
                    [groupe_nominal, [pronom_personnel, ["vous"]]],
                    [verbe_transitif, ["mangez"]]
                ]
            ],
            [groupe_verbal,
                [verbe_intransitif, ["meurent"]]
            ]
        ]
    ).

test('pronom personnel 2', [nondet]) :-
    analyse(
        ["nous", "regardons", "Michel", "qui", "mange", "une", "pomme"],
        [phrase,
            [groupe_nominal, [pronom_personnel, ["nous"]]],
            [groupe_verbal,
                [verbe_transitif, ["regardons"]],
                [groupe_nominal,
                    [nom_propre, ["Michel"]],
                    [relative,
                        [pronom_relatif_sujet, ["qui"]],
                        [groupe_verbal,
                            [verbe_transitif, ["mange"]],
                            [groupe_nominal,
                                [déterminant, ["une"]],
                                [nom_commun, ["pomme"]]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ).

test('pronom personnel 3', [fail]) :-
    analyse(
        ["je", "qui", "mange", "mange"],
        _
    ).

test('pronom personnel 4', [fail, fixme('gérer les pronoms personnels toniques')]) :-
    analyse(
        ["le", "chat", "écoute", "je"],
        _
    ).

test('adjectif possesif', [nondet]) :-
    analyse(
        ["Paul", "qui", "cherche", "les", "clés", "regarde", "dans", "son", "tiroir"],
        [phrase,
            [groupe_nominal,
                [nom_propre, ["Paul"]],
                [relative,
                    [pronom_relatif_sujet, ["qui"]],
                    [groupe_verbal,
                        [verbe_transitif, ["cherche"]],
                        [groupe_nominal,
                            [déterminant, ["les"]],
                            [nom_commun, ["clés"]]
                        ]
                    ]
                ]
            ],
            [groupe_verbal,
                [verbe_transitif, ["regarde"]],
                [préposition, ["dans"]],
                [groupe_nominal,
                    [déterminant, ["son"]],
                    [nom_commun, ["tiroir"]]
                ]
            ]
        ]
    ).

:- end_tests('analyse').
